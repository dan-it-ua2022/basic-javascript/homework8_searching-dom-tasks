//=================================================================
// 1) Найти все параграфы на странице и установить цвет фона #ff0000
let tagP = document.querySelectorAll('p');
tagP.forEach(item => item.style.backgroundColor = '#ff0000');

//=================================================================
// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.
let myID = document.querySelector('#optionsList');
console.log(myID);

let parentMyID = myID.parentElement;
console.log(parentMyID);

let childenMyID = myID.children;

if (childenMyID.length !== 0){
  [...childenMyID].forEach(item => {
    console.log(`name:${item.nodeName} | type:${item.nodeType}`);
  });
}

//=================================================================
// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>

//************************************************************************/
// задание неточное
// в html нет параграфа с классом testParagraph, есть с id=testParagraph
// контент - имеется ввиду текстовое содержимое или с учетом парсинга html
//************************************************************************/

// вариант 1.
// let myParagraph = document.querySelector('#testParagraph');
// let newMyParagraph = document.createElement('p');
// newMyParagraph.classList.add('testParagraph');
// newMyParagraph.innerText = '<p>This is a paragraph</p>';
// myParagraph.after(newMyParagraph);
// console.log(myParagraph);

// вариант 2.
// let myParagraph = document.querySelector('#testParagraph');
// myParagraph.innerText = '<p>This is a paragraph</p>';
// console.log(myParagraph);

// вариант 3.
let myParagraph = document.querySelector('#testParagraph');
myParagraph.innerHTML = '<p>This is a paragraph</p>';
console.log(myParagraph);

// вариант 4.
// let myParagraph = document.querySelector('#testParagraph');
// myParagraph.outerHTML = '<p>This is a paragraph</p>';
// console.log(myParagraph);

//=================================================================
// 4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item. 

let myMainHeader = document.querySelectorAll('.main-header li');
console.log(myMainHeader);
myMainHeader.forEach(item => {
  item.className = 'nav-item';
  console.log(item);
});

//=================================================================
// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов. 

let mySections = document.querySelectorAll('.section-title');
mySections.forEach(item => {
  item.classList.remove('section-title');
});
console.log(mySections);


