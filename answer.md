## Теоретичні питання
**1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)**

DOM - это объектная модель HTML документа (можно назвать - объект ???), создаваемая браузером при его чтении/загрузке/парсинге, где элементы HTML документа представляются объектами с свойствами и методами, согласно своему типу. DOM описывает HTML и предоставляет свойства и методы для управления содержимым документа<br>


**2. Какая разница между свойствами HTML-элементов innerHTML и innerText**

elem.innerHTML - позволяет вставлять/получать в/из elem содержимое как HTML код<br>
elem.innerText - позволяет вставлять/получать в/из elem текстовое значение elem<br>


**3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?**

elem.getElementById(id)<br>
elem.getElementsByTagName(tag)<br>
elem.getElementsByClassName(className)<br>
elem.getElementsByName(name)<br>

**предпочтительнее**<br>
elem.querySelector(css-selector)<br>
elem.querySelectorAll(css-selector)<br>

